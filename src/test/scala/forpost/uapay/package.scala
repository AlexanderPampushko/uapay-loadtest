package forpost

import io.gatling.http.Predef.{http, status}
import io.gatling.core.Predef._

package object uapay {

  val openCashdeskHeaders = Map(
    "Accept" -> "application/json, text/plain, */*",
    "Content-Type" -> "application/json;charset=UTF-8",
    "Accept-Encoding" -> "gzip, deflate",
    "Accept-Language" -> "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7",
    "Origin" -> "http://web.qa.forpost.local",
    "User-Agent" -> "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/64.0.3282.140 Chrome/64.0.3282.140 Safari/537.36",
    "Connection" -> "keep-alive",
    "Referer" -> "http  http://web.dev.forpost.local/?")

  val listallHeader = Map(
    "Accept" -> "application/json, text/plain, */*",
    "Content-Type" -> "application/json;charset=UTF-8",
    "Origin" -> "http://web.qa.forpost.local")

  val calculationCommissionHeader = Map(
    "Accept" -> "application/json, text/plain, */*",
    "Content-Type" -> "application/json;charset=UTF-8",
    "Origin" -> "http://web.qa.forpost.local")

  val headersSearchOperations = Map(
    "Origin" -> "http://web.qa.forpost.local",
    "Accept-Encoding" -> "gzip, deflate",
    "Accept-Language" -> "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7",
    "User-Agent" -> "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/64.0.3282.140 Chrome/64.0.3282.140 Safari/537.36",
    "Content-Type" -> "application/json;charset=UTF-8",
    "Accept" -> "application/json, text/plain, */*",
    "Referer" -> "http://web.qa.forpost.local/?",
    "Cookie" -> "connect.sid=s%3Aamnf0Up7iffXN5AZT9r8M9ouphQRcrc6.56JAfgKts%2FeFIHsEsoPjEe1BNzwJtO%2FNzFZSTOmdXRo",
    "Connection" -> "keep-alive"
  )

  val headersSendPayment = Map(
    "Accept" -> "application/json, text/plain, */*",
    "Referer" -> "http://web.qa.forpost.local/?",
    "Origin" -> "http://web.qa.forpost.local",
    "User-Agent" -> "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/64.0.3282.140 Chrome/64.0.3282.140 Safari/537.36",
    "Content-Type" -> "application/json;charset=UTF-8")
  val bodySendPayment = "{\"username\":\"kurochkin.ya\",\"amount\":1500,\"sender\":{\"cashdesk\":{\"city_id\":\"1\",\"branch_id\":\"11\",\"cashdesk_id\":\"111\",\"rro_registrate_num\":\"\"},\"person\":{\"contacts\":{\"phone\":\"+380973002577\"},\"personal\":{\"surname\":\"Іванов\",\"name\":\"Іван\",\"patronymic\":\"Іванович\"},\"identity\":{\"kind\":\"Паспорт громадянина України\",\"issue_office\":\"\",\"document_id\":\"\",\"issue_date\":\"\"},\"additional\":{\"birth_date\":\"\",\"birth_city\":\"\"}}},\"receiver\":{\"contacts\":{\"phone\":\"+380501289461\"},\"personal\":{\"surname\":\"Васильченко\",\"name\":\"Василь\",\"patronymic\":\"Васильович\"}}}"
  val pathSendPayment = "/payment/create"

  val bodySearchPayment = "{\"columns\":[\"transaction_id\",\"operation_id\",\"product_name\",\"transaction_type_name\",\"transaction_date\",\"document_number\",\"operation_type\",\"payment_tool_type\",\"pos_number\",\"payment_amount\",\"tax_amount\"],\"per_page\":15,\"current_page\":1,\"order_by\":\"\",\"order_type\":\"desc\",\"query\":\"65\",\"check_is_error\":null}"
  val pathSearchPayment = "/journal/filtered-records"

  val pathOpenCashdesk = "/cashdesk/open-cashdesk"

  val uriBackEnd = "http://app.dev.forpost.local:63001"

  val uriFrontEnd = "http://web.qa.forpost.local"

  val authorizationHeaders = Map(
    "Accept" -> "application/json, text/plain, */*",
    "Content-Type" -> "application/json;charset=UTF-8",
    "Origin" -> "http://web.qa.forpost.local")

  val loginPage = http("Страница логина")
    .get(uriFrontEnd +"/?#/login")
    .check(status.is(200))

  val authorization = http("Авторизация")
    .post(uriBackEnd + "/user/login")
    .headers(authorizationHeaders)
    .body(RawFileBody("Данные для авторизации пользователя.txt"))

    .check(status.is(200))

  val sentPayment = http("Отправка перевода")
    .post(uriBackEnd + pathSendPayment)
    .headers(headersSendPayment)
    .body(RawFileBody("Отправка перевода.txt"))

  val searchPayment = http("Поиск платежа")
    .post(uriBackEnd + pathSearchPayment)
    .headers(headersSearchOperations)
    .body(StringBody(bodySearchPayment))

  val calculationCommission = http("Post расчет комиссии")
    .post(uriBackEnd + "/payment/count-fee")
    .headers(calculationCommissionHeader)
    .body(RawFileBody("Расчет комиссии данные.txt"))

  val listall = http("Касса")
    .post(uriBackEnd + "/payment/listall")
    .headers(listallHeader)
    .body(RawFileBody("Касса.txt"))


  val openCashdesk = http("Открытие смены")
    .post(uriBackEnd + pathOpenCashdesk)
    .headers(openCashdeskHeaders)
    .body(RawFileBody("Открытие кассы.txt"))

//  val shiftStatus = http("")
}
