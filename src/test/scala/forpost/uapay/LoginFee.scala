package forpost.uapay

import scala.concurrent.duration._
import io.gatling.core.Predef._
import io.gatling.http.Predef.{http, _}
import io.gatling.jdbc.Predef._

class LoginFee extends Simulation {

  val httpProtocol = http
    .baseURL("http://app.dev.forpost.local:63001")
    .inferHtmlResources(BlackList(""".*\.css""", """.*\.gif""", """.*\.jpeg""", """.*\.jpg""", """.*\.ico""", """.*\.woff""", """.*\.(t|o)tf""", """.*\.png""", """http://localhost:8000/GetRROStatus"""), WhiteList())
    .acceptHeader("*/*")
    .acceptEncodingHeader("gzip, deflate")
    .acceptLanguageHeader("ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7")
    .userAgentHeader("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/64.0.3282.140 Chrome/64.0.3282.140 Safari/537.36")


  val authorization = http("Авторизация")
    .post(uriBackEnd + "/user/login")
    .headers(authorizationHeaders)
    .body(RawFileBody("Данные для авторизации пользователя.txt"))
    .check(headerRegex("Set-Cookie", "access_token=(.*);Version=*").saveAs("auth_token"));


  val workCashier = scenario("Работа кассира")
    .exec(loginPage)
    .exec(authorization)
    .exec(listall)
      randomSwitch(
        (40, exec(calculationCommission)),
        (40, exec(sentPayment)),
        (20, exec(searchPayment))
      )


  val openCashdeskTest = scenario("Открытие смены")
    .exec(loginPage)
    .exec(authorization)
    .exec(listall)
    .exec(openCashdesk)

  val loginSession = scenario("Логин в систему")
    .exec(loginPage)
    .exec(authorization)

  //    .exec(calculationCommission)
  //    .exec(sentPayment)
  //    .exec(searchPayment)


  setUp(
    workCashier.inject(nothingFor(11 minute),(rampUsers(5000) over (5 minute))).throttle(reachRps(20) in (1 second), holdFor(30 minute)).protocols(httpProtocol),
    openCashdeskTest.inject(nothingFor(10 minute),(atOnceUsers(1))).protocols(httpProtocol),
    loginSession.inject(heavisideUsers(5000) over (10 minute))).protocols(httpProtocol)
  //(nothingFor(3 seconds),atOnceUsers(5)).protocols(httpProtocol),

      //(rampUsers(2000) over (5 minute))).throttle(reachRps(20) in (1 second), holdFor(2 hour)).protocols(httpProtocol))
  //constantUsersPerSec(1) during(10 minute))).throttle(reachRps(300) in (2 minute),holdFor(1 minute))
  //		(heavisideUsers(10000) over(10 minute))) Плавный заход в систему нужного количества пользователей
  //			.throttle(reachRps(17) in (1 seconds),
  //		holdFor(1 minute)
  //	    )).maxDuration(2 minute)
  //(rampUsersPerSec(0.2) to (0.2) during(30 second))
}

